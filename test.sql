CREATE TABLE projects (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  description TEXT,
  status ENUM('awaiting-start', 'in-progress', 'on-hold', 'completed') NOT NULL DEFAULT 'awaiting-start',
  manager_id INT NOT NULL,
  engineer_id INT NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE milestones (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  description TEXT,
  project_id INT NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (project_id) REFERENCES projects(id)
);