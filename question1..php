<?php

function fibonacci($n) {
    // Returns the nth Fibonacci number
    if ($n <= 1) {
        return $n;
    } else {
        return fibonacci($n - 1) + fibonacci($n - 2);
    }
}

// Generate the array of numbers from 0 to 100
$array = range(0, 100);

// Sort the array in descending order
rsort($array);

// Create a new array of Fibonacci numbers starting from the seventh number
$frray = array();
for ($i = 6; $i < count($array); $i++) {
    $frray[] = fibonacci($array[$i]);
}

// Print the resulting array of Fibonacci numbers
print_r($frray);