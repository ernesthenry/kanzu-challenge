<?php
// import test.sql;

function createProject($name, $description, $manager_id, $engineer_id, $milestone_description) {
    // Validate inputs
    if (empty($name) || empty($manager_id) || empty($engineer_id) || empty($milestone_description)) {
        return array('success' => false, 'message' => 'Invalid input');
    }

    // Insert project into database
    $conn = mysqli_connect('localhost', 'username', 'password', 'database');
    $query = "INSERT INTO projects (name, description, manager_id, engineer_id) VALUES ('$name', '$description', $manager_id, $engineer_id)";
    mysqli_query($conn, $query);
    $project_id = mysqli_insert_id($conn);
}

//Establish database connection
$mysqli = new mysqli("localhost", "username", "password", "database");

// Check for connection errors
if ($mysqli->connect_error) {
  die("Connection failed: " . $mysqli->connect_error);
}

// Create a new project and add milestones
function create_project($project_name, $developer_id, $milestones) {
  global $mysqli;

  // Create new project
  $sql = "INSERT INTO projects (project_name, developer_id, status) VALUES ('$project_name', $developer_id, 'awaiting-start')";
  $mysqli->query($sql);

  // Get the ID of the newly created project
  $project_id = $mysqli->insert_id;

  // Add milestones to the project
  foreach ($milestones as $milestone) {
    $name = $mysqli->real_escape_string($milestone['name']);
    $description = $mysqli->real_escape_string($milestone['description']);
    $sql = "INSERT INTO milestones (project_id, name, description, status) VALUES ($project_id, '$name', '$description', 'awaiting-start')";
    $mysqli->query($sql);
  }

  return $project_id;
}

// Change the status of a project
function change_project_status($project_id, $status) {
  global $mysqli;

  // Get the current status of the project
  $sql = "SELECT status FROM projects WHERE project_id = $project_id";
  $result = $mysqli->query($sql);
  $row = $result->fetch_assoc();
  $current_status = $row['status'];

  // Check if the status can be changed
  if ($current_status != 'completed') {
    $sql = "UPDATE projects SET status = '$status' WHERE project_id = $project_id";
    $mysqli->query($sql);
    return true;
  } else {
    return false;
  }
}

// Get a list of projects assigned to an engineer
function get_projects_for_engineer($engineer_id) {
  global $mysqli;

  $sql = "SELECT * FROM projects WHERE developer_id = $engineer_id";
  $result = $mysqli->query($sql);
  $projects = array();
  while ($row = $result->fetch_assoc()) {
    $projects[] = $row; 

  }}

  
// POST /projects - Create a new project
// POST /projects/{projectId}/milestones - Add a milestone to a project
// PUT /projects/{projectId}/status - Change the status of a project
// GET /projects?engineer={engineerId} - Get a list of projects assigned to an engineer
// PUT /projects/{projectId}/engineer - Change the engineer assigned to a project
// PUT /projects/{projectId}/complete - Mark a project as completed




