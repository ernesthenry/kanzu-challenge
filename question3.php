<?php

$file="./test-file.txt";

// Function that reads the text from the file and returns an array without any duplicate items:
function get_unique_words($file_path) {
    // Read the contents of the file and split it into an array of words
    $text = file_get_contents($file_path);
    $words = preg_split('/\s+/', $text);

    // Remove duplicate words and return the resulting array
    return array_unique($words);
}

$unique_words = get_unique_words($file);
print_r($unique_words);


//  Function that reads the text from the file and returns an array with only the punctuation marks:
function get_punctuation_marks($file_path) {
    // Read the contents of the file and remove all non-punctuation characters
    $text = file_get_contents($file_path);
    $punctuation = preg_replace('/[^\p{P}\p{S}]/u', '', $text);

    // Split the punctuation characters into an array and return it
    return preg_split('//u', $punctuation, -1, PREG_SPLIT_NO_EMPTY);
}

$punctuation_marks = get_punctuation_marks("test-file.txt");
print_r($punctuation_marks);

