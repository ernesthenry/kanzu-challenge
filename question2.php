<?php

$paragraph = 'This is a paragraph and it has to find 256781123456, testemail@gmail.com
and https://kanzucode.com/';


//  Functions that use regular expressions:
// 
// Function to extract the email address 
function extract_email_address_with_regex($paragraph) {
    preg_match("/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/", $paragraph, $matches);
    return $matches[0];
}

// Function to extract the phone number
function extract_phone_number_with_regex($paragraph) {
    preg_match("/\d{9,}/", $paragraph, $matches);
    return $matches[0];
}

// Function to extract the URL
function extract_url_with_regex($paragraph) {
    preg_match("/https?:\/\/[\w\-\.!~?&=+\*\'\(\),]+/", $paragraph, $matches);
    return $matches[0];
}

// Functions that do not use regular expressions:

// Function to extract the email address:
function extract_email_address($paragraph) {
    $at_symbol_pos = strpos($paragraph, '@');
    $dot_symbol_pos = strpos($paragraph, '.', $at_symbol_pos);
    $email_address = substr($paragraph, $at_symbol_pos + 1, $dot_symbol_pos - $at_symbol_pos - 1);
    return $email_address;
}

// Function to extract the phone number:
function extract_phone_number($paragraph) {
    $phone_number = '';
    $numbers = str_split($paragraph);
    foreach($numbers as $number) {
        if (is_numeric($number)) {
            $phone_number .= $number;
        }
    }
    return $phone_number;
}

// Function to extract the URL:
function extract_url($paragraph) {
    $start_pos = strpos($paragraph, 'http');
    $end_pos = strpos($paragraph, ' ', $start_pos);
    $url = substr($paragraph, $start_pos, $end_pos - $start_pos);
    return $url;
}

